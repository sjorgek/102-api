package com.mastertech.marketplace.controllers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mastertech.marketplace.models.Offer;
import com.mastertech.marketplace.models.Product;
import com.mastertech.marketplace.repositories.OfferRepository;
import com.mastertech.marketplace.repositories.ProductRepository;

import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.Optional;

@RunWith(SpringRunner.class)
@WebMvcTest(OfferController.class)
public class OfferControllerTest {

	@Autowired
	MockMvc mockMvc;
	
	@MockBean 
	ProductRepository productRepository;
	
	@MockBean
	OfferRepository offerRepository;
	
	ObjectMapper mapper = new ObjectMapper();
	
	Product product;
	Offer offer;
	
	@Before
	public void prepareTests() {
		product = new Product();
		product.setId(1);
		product.setPrice(100.0);
		
		Optional<Product> productQuery = Optional.of(product);
		
		when(productRepository.findById(1)).thenReturn(productQuery);
		
		offer = new Offer();
		offer.setId(1);
		offer.setProduct(product);
		
		when(offerRepository.save(any(Offer.class))).thenReturn(offer);
	}
	
	@Test
	public void shouldCreateOffer() throws Exception {
		Offer testOffer = new Offer();
		testOffer.setBid(100.0);
		testOffer.setProduct(product);
		
		String json = mapper.writeValueAsString(testOffer);
		
		System.out.println(json);
		
		mockMvc.perform(post("/offers")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().is(201));

	}
	
	@Test
	public void shouldNotCreateOfferWithLowBid() throws Exception {
		Offer testOffer = new Offer();
		testOffer.setBid(10.0);
		testOffer.setProduct(product);
		
		String json = mapper.writeValueAsString(testOffer);
		
		mockMvc.perform(post("/offers")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().is(400));

	}
	
}
