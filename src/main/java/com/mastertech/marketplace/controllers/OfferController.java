package com.mastertech.marketplace.controllers;

import java.net.URI;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mastertech.marketplace.helpers.URIBuilder;
import com.mastertech.marketplace.models.Offer;
import com.mastertech.marketplace.models.Product;
import com.mastertech.marketplace.repositories.OfferRepository;
import com.mastertech.marketplace.repositories.ProductRepository;

@RestController
@RequestMapping("/offers")
public class OfferController {
	@Autowired
	OfferRepository offerRepository;

	@Autowired
	ProductRepository productRepository;

	@GetMapping
	public Iterable<?> getAll() {
		return offerRepository.findAll();
	}

	@GetMapping(path="/{id}")
	public ResponseEntity<?> get(@PathVariable int id) {
		Optional<Offer> offerQuery = offerRepository.findById(id);
		
		if(offerQuery.isPresent()) {
			Offer offer = offerQuery.get();
			return ResponseEntity.ok(offer);
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@GetMapping(path="/products/{id}")
	public ResponseEntity<?> getAllFromProduct(@PathVariable int id) {
		Optional<Product> productQuery = productRepository.findById(id);
		
		if(!productQuery.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		
		Product product = productQuery.get();
		Iterable<Offer> offers = offerRepository.getAllByProduct(product);
		
		return ResponseEntity.ok(offers);
	}
	
	@PostMapping
	public ResponseEntity<?> create(@RequestBody Offer offer) {
		Optional<Product> productQuery = productRepository.findById(offer.getProduct().getId());
		
		if(! productQuery.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		
		Offer savedOffer = offerRepository.save(offer);
		
		URI uri = URIBuilder.fromString("/offer/" + savedOffer.getId());
		
		return ResponseEntity.created(uri).build();
	}
	
	@PutMapping("/approve/{id}")
	public ResponseEntity<?> accept(@PathVariable int id) {
		Optional<Offer> offerQuery = offerRepository.findById(id);
		
		if(! offerQuery.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		Offer offer = offerQuery.get();
		
		offer.setApproved(true);
		
		offerRepository.save(offer);
		
		return ResponseEntity.ok().build();
	}
}
